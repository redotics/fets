import os
import sys
import pandas as pd
from datetime import datetime
import pytest
from sklearn.pipeline import Pipeline
sys.path.insert(
    0,
    os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from fets.date import TSNumericalToDate

null_date = datetime(1800, 1, 1)

def test_inits():
    tr_a = TSNumericalToDate()
    tr_b = TSNumericalToDate(past_limit=datetime(2001, 1, 1))
    tr_c = TSNumericalToDate(future_limit=datetime(2101, 1, 2))
    tr_d = TSNumericalToDate(fill_date=datetime(2102, 1, 2))

    assert tr_a.future_limit is None
    assert tr_a.past_limit is None
    assert tr_a.fill_date == null_date
    assert tr_b.future_limit is None
    assert tr_b.past_limit == datetime(2001, 1, 1)
    assert tr_b.fill_date == null_date
    assert tr_c.future_limit == datetime(2101, 1, 2)
    assert tr_c.past_limit is None
    assert tr_c.fill_date == null_date
    assert tr_d.future_limit is None
    assert tr_d.past_limit is None
    assert tr_d.fill_date == datetime(2102, 1, 2)


def test_no_limits():
    data_list = [2014, 201409.0, 23, 20130618.0, -99]
    test_ts = pd.Series(data_list)

    pipeline = Pipeline([
        ("to_datetime", TSNumericalToDate())
    ])

    result = pipeline.transform(test_ts)

    assert isinstance(result, pd.Series)
    assert result.shape[0] == 5
    assert result[0] == datetime(2014, 1, 1)
    assert result[1] == datetime(2014, 9, 1)
    assert result[2] == null_date
    assert result[3] == datetime(2013, 6, 18)
    assert result[4] == null_date


def test_with_limits():
    data_list = [1998, 201409.0, 23, 20330618.0, -99]
    test_ts = pd.Series(data_list)

    pipeline = Pipeline([
        ("to_datetime", TSNumericalToDate(datetime(2014, 1, 2), datetime(2018, 6, 18)))
    ])

    result = pipeline.transform(test_ts)

    assert isinstance(result, pd.Series)
    assert result.shape[0] == 5
    assert result[0] == datetime(2014, 1, 2)
    assert result[1] == datetime(2014, 9, 1)
    assert result[2] == null_date
    assert result[3] == datetime(2018, 6, 18)
    assert result[4] == null_date


def test_with_bad_limits():
    data_list = [1998, 201409.0, 23, 20330618.0, -99]
    test_ts = pd.Series(data_list)

    with pytest.raises(TypeError) as excinfo:
        pipeline = Pipeline([
            ("to_datetime", TSNumericalToDate("20A4-0C-0D", datetime(2018, 6, 18)))
        ])

        result = pipeline.transform(test_ts)


def test_unused_fit():
    data_list = [1998, 201409.0, 23, 20330618.0, -99]
    test_ts = pd.Series(data_list)

    pipeline = Pipeline([
        ("to_datetime", TSNumericalToDate(datetime(2014, 1, 2), datetime(2018, 6, 18)))
    ])

    result = pipeline.fit(test_ts)

    # Nothing happened 
    assert isinstance(result, Pipeline)


def test_wrong_input():
    data_list = [1998, 201409.0, 23, 20330618.0, -99]

    tr = TSNumericalToDate(datetime(2014, 1, 2), datetime(2018, 6, 18))

    # There are proper ways to test exception with pytest
    error_n = 0
    try:
        result = tr.transform(data_list)
    except TypeError as eee:
        error_n = 1

    assert error_n == 1
