import pandas as pd
import numpy as np
from sklearn.pipeline import Pipeline
from fets.math import TSIntegrale


# Creating dummy data
index = pd.date_range("2019-07-01", "2019-07-10", freq="1H")
print("Time index created with {} entries".format(len(index)))
print("Index time step is: {}".format(index[1]-index[0]))

# Creating a sinusoidal signal
S = pd.Series(index=index,
              data=np.sin(np.linspace(0, 100*3.14, num=len(index))))
print("Tail the input series: ")
print(S.tail(10))

# Creating pipeline using FETS transformers
pipeline = Pipeline([
    ("integrale", TSIntegrale("5H"))
])

# Applying the transformation to our Series
result = pipeline.transform(S)
print("Tail of the results: ")
print(result.tail(10))
