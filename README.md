FETS
=====

[![pipeline status](https://gitlab.com/redsharpbyte/fets/badges/master/pipeline.svg)](https://gitlab.com/redsharpbyte/fets/commits/master)
[![coverage report](https://gitlab.com/redsharpbyte/fets/badges/master/coverage.svg)](https://gitlab.com/redsharpbyte/fets/commits/master)



Set of ready-to-use transformers for your feature engineering pipelines in scikit-learn.

Inspired by the number of times I had to rewrite transformers and by the number 
of times we all did exactly the same.


How-To
======

See [examples/](https://gitlab.com/redsharpbyte/fets/tree/master/examples)

Installation
============

```bash
pip install fets
```

Testing
=======

```bash
cd fets/
pytest -v tests
```
